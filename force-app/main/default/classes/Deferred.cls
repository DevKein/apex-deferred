public virtual class Deferred Implements Queueable, Database.AllowsCallouts {

    protected List<Deferred.Promise> deferredCluster = new List<Deferred.Promise>();
    protected Object DeferredData;
    protected Deferred.Error errorHandler;
    protected Deferred.Done callbackHandler;

    private Map<String,Object> storedParams = new Map<String,Object>();
  
    public Deferred(Deferred.Promise deferred) {
      then(deferred);
    }
  
    public Deferred then(Deferred.Promise deferred) {
      deferredCluster.add(deferred);
      storedParams.putAll(deferred.getStoredParams());
      return this;
    }
  
    public Deferred error(Deferred.Error errorHandler) {
      this.errorHandler = errorHandler;
      return this;
    }
  
    public Deferred callback(Deferred.Callback callbackHandler) {
      this.callbackHandler = callbackHandler;
      return this;
    }

    //Execute Deferred
    public Void execute(Object input) {
      DeferredData = input;
      System.enqueueJob(this);
    }
  
    public Void execute() {
      System.enqueueJob(this);
    }
  
    public Void execute(QueueableContext context) {
      try {
        Deferred.Promise currentDeferred = deferredCluster.remove(0);
        DeferredData = currentDeferred.resolve(DeferredData);
        if (deferredCluster.size() > 0) {
          System.enqueueJob(this);
          return;
        }
      } catch (Exception e) {
        DeferredData = errorHandler.error(e);
      }
      callbackHandler.callback(DeferredData);
    }
    
    // Inner Interfaces
    public Interface Promise {
      object resolve(Object input);
      void setStoredParams(String key, Object input);
      Map<String,Object> getStoredParams();
    }
  
    public Interface Error {
      object error(Exception e);
    }
  
    public Interface Callback {
      void callback(Object input);
    }
  
  }