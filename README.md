Usage Example:

```apex
public class ValidateBeforeDeploy {
  public void validate() {
      new Promise(new QueueA())
        .then(new QueueB())
        .error(new validateErrorHandler()) //implements Deferred.Error
        .done(new validateCallback()) //implements Deferred.Callback
        .execute();
  }

  public void validate(String param) {
      new Promise(new QueueA())
        .then(new QueueB())
        .error(new validateErrorHandler()) //implements Deferred.Error
        .done(new validateCallback()) //implements Deferred.Callback
        .execute(param);
  }
}
```